const imagesContainer = document.getElementById("images");
let timestamp = Date.now();
let errCount = 0;

const fetchLatestImages = () => {
    setInterval(() => {
        fetch("/latest", {
        //code goes here
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({'timeOfFetchRequest':timestamp})
        
        })
        .then(res => res.json())
        .then(res => {
            timestamp = res.timestamp
            for(let image of res.imagesArray){
                let imgElement = document.createElement('img')
                imgElement.src = '/uploads/'+image
                document.body.prepend(imgElement)
            }
        })
        .catch(err => {
            console.log('ERROR')
            errCount++
        })
    }, 5000);
}

fetchLatestImages();

// the .then on line 18: 
// - get back an images array from the sever, loop through that array and create a new <img> tag for each new image.
// then append that new <img> to the webpage - - document.createElement('img') document.getElementById('someId') element.appendChild
// - overwrite our client timestamp with whatever timestamp value the server sent back to us